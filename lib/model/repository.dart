import 'dart:convert';

import 'package:covid19apps/model/modelcovid.dart';
import 'package:http/http.dart' as http;

Future<covidApi> getCovidApi() async {
  final Uri uri =
      Uri.parse("https://apicovid19indonesia-v2.vercel.app/api/indonesia");
  final response = await http.get(uri);

  if (response.statusCode == 200) {
    final jsonStudent = jsonDecode(response.body);
    return covidApi.fromJson(jsonStudent);
  } else {
    throw Exception();
  }
}
