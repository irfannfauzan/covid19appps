import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(200),
          child: AppBar(
            centerTitle: true,
            title: Text("Apps"),
            backgroundColor: Color(0xFFFEECB5),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50),
                    bottomLeft: Radius.circular(50))),
          ),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              height: 60,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 25),
              height: 250,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Color(0xFFE5E5E5)),
              ),
              child: Text(
                "Aplikasi Demo Covid-19 ini dapat membantu masyarakat dalam mencari data penyebaran Covid-19 di indonesia. Dan dapat mengedukasi masyarakat indonesia agar lebih taat dengan protokol kesehatan.  ",
                style: TextStyle(fontSize: 20),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ));
  }
}
