import 'package:covid19apps/model/modelcovid.dart';
import 'package:covid19apps/model/repository.dart';
import 'package:flutter/material.dart';

class DataCovid extends StatefulWidget {
  @override
  _DataCovidState createState() => _DataCovidState();
}

class _DataCovidState extends State<DataCovid> {
  Container _covid(String imageVal, String dirawat, String sembuh) {
    return Container(
      width: 160,
      child: Card(
        child: Wrap(
          children: <Widget>[
            Image.asset(imageVal),
            FittedBox(
              fit: BoxFit.cover,
            ),
            ListTile(
              title: Text(
                dirawat,
                textAlign: TextAlign.center,
              ),
              subtitle: Text(
                sembuh,
                textAlign: TextAlign.center,
                style:
                    TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
              ),
            )
          ],
        ),
      ),
    );
  }

  Container _posterCovid(String imageVal) {
    return Container(
      height: 150,
      child: Card(
        child: Wrap(
          children: <Widget>[
            Image.asset(imageVal),
            // FittedBox(
            //   fit: BoxFit.cover,
            // )
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(200),
          child: AppBar(
            centerTitle: true,
            title: Text("Apps"),
            backgroundColor: Color(0xFFFEECB5),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50),
                    bottomLeft: Radius.circular(50))),
          ),
        ),
        body: ListView(children: <Widget>[
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 25),
            height: 55,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: Icon(Icons.location_on_sharp),
              trailing: Icon(Icons.keyboard_arrow_down),
              title: Text("Indonesia"),
              onTap: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text("Coming Soon!"),
                      content: Text("I’m sorry for the inconvenience"),
                      actions: [
                        ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text("OK"))
                      ],
                    );
                  },
                );
              },
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 25),
            child: Text(
              "Update Data Covid-19",
              style: TextStyle(color: Colors.red, fontSize: 15),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            child: FutureBuilder<covidApi>(
              future: getCovidApi(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final _covidApi = snapshot.data;
                  return Container(
                    margin: EdgeInsets.symmetric(horizontal: 22),
                    height: 250,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        _covid("images/blue.png", "Positif :",
                            "${_covidApi?.positif}"),
                        _covid("images/green.png", "Sembuh :",
                            "${_covidApi?.sembuh}"),
                        _covid("images/red.png", "Meninggal : ",
                            "${_covidApi?.meninggal}"),
                        _covid("images/yellow.png", "Dirawat : ",
                            "${_covidApi?.dirawat}"),
                      ],
                    ),
                  );
                } else if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                }

                return CircularProgressIndicator();
              },
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 25),
            child: Text(
              "Maps Covid-19",
              style: TextStyle(color: Colors.red, fontSize: 15),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 22),
            height: 250,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                _posterCovid("images/maps21.jpg"),
              ],
            ),
          ),
          Container(
            height: 20,
          )
        ]));
  }
}




   // body: ListView(
            //   children: <Widget>[
            //     Container(
            //       height: 20,
            //     ),
            //     Container(
            //       margin: EdgeInsets.symmetric(horizontal: 20),
            //       height: 60,
            //       width: double.infinity,
            //       decoration: BoxDecoration(
            //         color: Colors.white,
            //         borderRadius: BorderRadius.circular(20),
            //         border: Border.all(color: Color(0xFFE5E5E5)),
            //       ),
            //       child: Row(
            //         children: <Widget>[
            //           SizedBox(
            //             width: 15,
            //           ),
            //           Icon(Icons.location_on_outlined),
            //           SizedBox(
            //             width: 20,
            //           ),
            //           Text("Location"),
            //           SizedBox(
            //             width: 170,
            //           ),
            //           Icon(Icons.keyboard_arrow_right_outlined),
            //         ],
            //       ),
            //     ),
            //     Container(
            //       height: 20,
            //     ),
            //     Container(
            //       margin: EdgeInsets.symmetric(horizontal: 20),
            //       height: 60,
            //       width: double.infinity,
            //       decoration: BoxDecoration(
            //         color: Colors.white,
            //         borderRadius: BorderRadius.circular(20),
            //         border: Border.all(color: Color(0xFFE5E5E5)),
            //       ),
            //       child: Row(
            //         children: <Widget>[
            //           SizedBox(
            //             width: 15,
            //           ),
            //           Icon(Icons.location_on_outlined),
            //           SizedBox(
            //             width: 20,
            //           ),
            //           Text("Location"),
            //           SizedBox(
            //             width: 170,
            //           ),
            //           Icon(Icons.keyboard_arrow_right_outlined),
            //         ],
            //       ),
            //     ),
            //     Container(
            //       height: 20,
            //     ),
            //     Container(
            //       margin: EdgeInsets.symmetric(horizontal: 20),
            //       height: 60,
            //       width: double.infinity,
            //       decoration: BoxDecoration(
            //         color: Colors.white,
            //         borderRadius: BorderRadius.circular(20),
            //         border: Border.all(color: Color(0xFFE5E5E5)),
            //       ),
            //       child: Row(
            //         children: <Widget>[
            //           SizedBox(
            //             width: 15,
            //           ),
            //           Icon(Icons.location_on_outlined),
            //           SizedBox(
            //             width: 20,
            //           ),
            //           Text("Location"),
            //           SizedBox(
            //             width: 170,
            //           ),
            //           Icon(Icons.keyboard_arrow_right_outlined),
            //         ],
            //       ),
            //     ),
            //     Container(
            //       height: 20,
            //     ),
            //     Container(
            //       margin: EdgeInsets.symmetric(horizontal: 20),
            //       height: 60,
            //       width: double.infinity,
            //       decoration: BoxDecoration(
            //         color: Colors.white,
            //         borderRadius: BorderRadius.circular(20),
            //         border: Border.all(color: Color(0xFFE5E5E5)),
            //       ),
            //       child: Row(
            //         children: <Widget>[
            //           SizedBox(
            //             width: 15,
            //           ),
            //           Icon(Icons.location_on_outlined),
            //           SizedBox(
            //             width: 20,
            //           ),
            //           Text("Location"),
            //           SizedBox(
            //             width: 170,
            //           ),
            //           Icon(Icons.keyboard_arrow_right_outlined),
            //         ],
            //       ),
            //     ),
            //   ],
            // ));