import 'package:covid19apps/screen/about.dart';
import 'package:covid19apps/screen/content.dart';
import 'package:covid19apps/screen/datacovid.dart';
import 'package:flutter/material.dart';

class BottomNavbar extends StatefulWidget {
  const BottomNavbar({Key? key}) : super(key: key);

  @override
  _BottomNavbarState createState() => _BottomNavbarState();
}

class _BottomNavbarState extends State<BottomNavbar> {
  int _selectedItems = 0;
  final List<Widget> _children = [DataCovid(), Content(), About()];

  void onTappedBar(int index) {
    setState(() {
      _selectedItems = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _children[_selectedItems],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTappedBar,
          currentIndex: _selectedItems,
          selectedItemColor: Color(0xFFAFE2DD),
          items: [
            BottomNavigationBarItem(label: "Home", icon: new Icon(Icons.home)),
            BottomNavigationBarItem(
                label: "Virus", icon: new Icon(Icons.hotel_rounded)),
            BottomNavigationBarItem(
                label: "About", icon: new Icon(Icons.person)),
          ],
        ));
  }
}
