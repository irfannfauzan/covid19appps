import 'package:flutter/material.dart';

class InfectedPage extends StatelessWidget {
  const InfectedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(200),
        child: AppBar(
          centerTitle: true,
          title: Text("Apps"),
          backgroundColor: Color(0xFFFEECB5),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50),
                  bottomLeft: Radius.circular(50))),
        ),
      ),
      body: ListView(children: <Widget>[
        Container(
          height: 20,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          height: 80,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Color(0xFFE5E5E5)),
          ),
          child: ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("images/infected1.jpg"),
            ),
            title: Text(
                "Selalu membersihkan dan menjaga ruangan dengan menggunakan disinfektan."),
          ),
        ),
        Container(
          height: 20,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          height: 80,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Color(0xFFE5E5E5)),
          ),
          child: ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("images/infected2.jpg"),
            ),
            title: Text(
                "Membuang tissue yang sudah digunakan kedalam tempat sampah."),
          ),
        ),
        Container(
          height: 20,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          height: 80,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Color(0xFFE5E5E5)),
          ),
          child: ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("images/infected3.jpg"),
            ),
            title: Text(
                "Selalu menjaga hidung dan mulut menggunakan tissue ketika bersin."),
          ),
        ),
        Container(
          height: 20,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          height: 80,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Color(0xFFE5E5E5)),
          ),
          child: ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("images/upaya3.jpg"),
            ),
            title: Text(
                "Hindari interaksi langsung dengan orang diluar. Berinteraksi dengan Telpon atau SMS."),
          ),
        ),
        Container(
          height: 20,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          height: 80,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Color(0xFFE5E5E5)),
          ),
          child: ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("images/logo3.jpg"),
            ),
            title: Text(
                "iwajibkan isolasi dirumah atau rumah sakit. Tidak diperbolehkan berpergian."),
          ),
        ),
        Container(
          height: 20,
        ),
      ]),
    );
  }
}
