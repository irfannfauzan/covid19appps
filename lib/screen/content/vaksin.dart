import 'package:covid19apps/model/modelvaksin.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class VaksinPage extends StatefulWidget {
  const VaksinPage({Key? key}) : super(key: key);

  @override
  _VaksinPageState createState() => _VaksinPageState();
}

class _VaksinPageState extends State<VaksinPage> {
  List<VaksinApi> vaksinApi = [];

  Future<bool> getData() async {
    final Uri uri = Uri.parse("https://reqres.in/api/users?page=1");
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      final result = apiVaksinFromJson(response.body);
      vaksinApi = result.data;
      print(vaksinApi);
      setState(() {});
      return true;
    } else {
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(200),
        child: AppBar(
          centerTitle: true,
          title: Text("Dummy's Api"),
          backgroundColor: Color(0xFFFEECB5),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50),
                  bottomLeft: Radius.circular(50))),
        ),
      ),
      body: ListView.separated(
          itemBuilder: (context, index) {
            final res = vaksinApi[index];
            SizedBox(
              height: 20,
            );
            return Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              height: 70,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Color(0xFFE5E5E5)),
              ),
              child: ListTile(
                leading: Text(res.id.toString()),
                title: Text(res.firstName + " " + res.lastName),
                subtitle: Text(res.email),
                trailing:
                    CircleAvatar(backgroundImage: NetworkImage(res.avatar)),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
          itemCount: vaksinApi.length),
    );
  }
}
