import 'package:flutter/material.dart';

class ProtectPage extends StatefulWidget {
  const ProtectPage({Key? key}) : super(key: key);

  @override
  _ProtectPageState createState() => _ProtectPageState();
}

class _ProtectPageState extends State<ProtectPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(200),
          child: AppBar(
            centerTitle: true,
            title: Text("Apps"),
            backgroundColor: Color(0xFFFEECB5),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50),
                    bottomLeft: Radius.circular(50))),
          ),
        ),
        body: ListView(children: <Widget>[
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/upaya1.jpg"),
              ),
              title: Text(
                  "Mencuci tangan adalah cara paling mudah mencegah bakteri dan virus."),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/upaya2.jpg"),
              ),
              title: Text(
                  "Memakai masker bermanfaat sangat besar untuk mencegah penularan virus corona. "),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/upaya3.jpg"),
              ),
              title: Text(
                  "Jaga jarak salah satu cara mencegah penyebaran virus corona"),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/upaya4.jpg"),
              ),
              title: Text(
                  "Hindari kerumunan atau tempat ramai, agar selalu terhindar dari penyebaran virus corona."),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/upaya5.jpg"),
              ),
              title: Text(
                  "Hindari menggunakan transportasi umum guna menghindari kerumunan."),
            ),
          ),
          Container(
            height: 20,
          ),
        ]));
  }
}
