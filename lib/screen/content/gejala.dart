import 'package:flutter/material.dart';

class GejalaPage extends StatefulWidget {
  const GejalaPage({Key? key}) : super(key: key);

  @override
  _GejalaPageState createState() => _GejalaPageState();
}

class _GejalaPageState extends State<GejalaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(200),
          child: AppBar(
            centerTitle: true,
            title: Text("Apps"),
            backgroundColor: Color(0xFFFEECB5),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50),
                    bottomLeft: Radius.circular(50))),
          ),
        ),
        body: ListView(children: <Widget>[
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/gejala1.jpg"),
              ),
              title: Text(
                  "Gejala pertama adalah Demam. Demam dengan Suhu 39 Derajat Celsius - 40 Derajat Celsius."),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/gejala2.jpg"),
              ),
              title: Text(
                  "Gejala kedua adalah Batuk. Dikarenakan infeksi virus menyebar ke dalam tubuh "),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/gejala3.jpg"),
              ),
              title: Text(
                  "Gejala ketiga adalah sesak nafas. Sesak nafas mungkin ringan dan tidak berlangsung lama"),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/gejala4.jpg"),
              ),
              title: Text(
                  "Gejala keempat adalah Sakit tenggorokan. Terjadi saat virus masuk ke dalam tubuh."),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/gejala5.jpg"),
              ),
              title: Text(
                  "Gejala kelima adalah sakit kepala dan nyeri otot. Terjadi saat virus masuk ke dalam tubuh "),
            ),
          ),
          Container(
            height: 20,
          ),
        ]));
  }
}
