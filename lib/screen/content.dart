import 'package:covid19apps/screen/content/gejala.dart';
import 'package:covid19apps/screen/content/protect.dart';
import 'package:covid19apps/screen/content/upaya.dart';
import 'package:covid19apps/screen/content/vaksin.dart';
import 'package:flutter/material.dart';

class Content extends StatefulWidget {
  @override
  _ContentState createState() => _ContentState();
}

class _ContentState extends State<Content> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(200),
          child: AppBar(
            centerTitle: true,
            title: Text("Apps"),
            backgroundColor: Color(0xFFFEECB5),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50),
                    bottomLeft: Radius.circular(50))),
          ),
        ),
        body: ListView(children: <Widget>[
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 53,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/logo1.jpg"),
              ),
              trailing: Icon(Icons.keyboard_arrow_right_outlined),
              title: Text("Protect Covid-19"),
              onTap: () {
                setState(() {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => ProtectPage()));
                });
              },
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 53,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/logo2.jpg"),
              ),
              trailing: Icon(Icons.keyboard_arrow_right_outlined),
              title: Text("Symptom Covid-19"),
              onTap: () {
                setState(() {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => GejalaPage()));
                });
              },
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 53,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/logo3.jpg"),
              ),
              trailing: Icon(Icons.keyboard_arrow_right_outlined),
              title: Text("What Should I Do ?"),
              onTap: () {
                setState(() {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => InfectedPage()));
                });
              },
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 53,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xFFE5E5E5)),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/upaya5.jpg"),
              ),
              trailing: Icon(Icons.keyboard_arrow_right_outlined),
              title: Text("Vaccinated"),
              onTap: () {
                setState(() {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => VaksinPage()));
                });
              },
            ),
          ),
        ]));
  }
}

// class _ContentState extends State<Content> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: PreferredSize(
//           preferredSize: Size.fromHeight(200),
//           child: AppBar(
//             centerTitle: true,
//             title: Text("Apps"),
//             backgroundColor: Color(0xFFFEECB5),
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.only(
//                     bottomRight: Radius.circular(50),
//                     bottomLeft: Radius.circular(50))),
//           ),
//         ),
//         body: ListView(
//           children: <Widget>[
//             Container(
//               height: 20,
//             ),
//             Container(
//               margin: EdgeInsets.symmetric(horizontal: 20),
//               height: 60,
//               width: double.infinity,
//               decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.circular(20),
//                 border: Border.all(color: Color(0xFFE5E5E5)),
//               ),
//               child: Row(
//                 children: <Widget>[
//                   SizedBox(
//                     width: 15,
//                   ),
//                   Icon(Icons.location_on_outlined),
//                   SizedBox(
//                     width: 20,
//                   ),
//                   Text("Location"),
//                   SizedBox(
//                     width: 170,
//                   ),
//                   Icon(Icons.keyboard_arrow_right_outlined),
//                 ],
//               ),
//             ),
//             Container(
//               height: 20,
//             ),
//             Container(
//               margin: EdgeInsets.symmetric(horizontal: 20),
//               height: 60,
//               width: double.infinity,
//               decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.circular(20),
//                 border: Border.all(color: Color(0xFFE5E5E5)),
//               ),
//               child: Row(
//                 children: <Widget>[
//                   SizedBox(
//                     width: 15,
//                   ),
//                   Icon(Icons.location_on_outlined),
//                   SizedBox(
//                     width: 20,
//                   ),
//                   Text("Location"),
//                   SizedBox(
//                     width: 170,
//                   ),
//                   Icon(Icons.keyboard_arrow_right_outlined),
//                 ],
//               ),
//             ),
//             Container(
//               height: 20,
//             ),
//             Container(
//               margin: EdgeInsets.symmetric(horizontal: 20),
//               height: 60,
//               width: double.infinity,
//               decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.circular(20),
//                 border: Border.all(color: Color(0xFFE5E5E5)),
//               ),
//               child: Row(
//                 children: <Widget>[
//                   SizedBox(
//                     width: 15,
//                   ),
//                   Icon(Icons.location_on_outlined),
//                   SizedBox(
//                     width: 20,
//                   ),
//                   Text("Location"),
//                   SizedBox(
//                     width: 170,
//                   ),
//                   Icon(Icons.keyboard_arrow_right_outlined),
//                 ],
//               ),
//             ),
//             Container(
//               height: 20,
//             ),
//             Container(
//               margin: EdgeInsets.symmetric(horizontal: 20),
//               height: 60,
//               width: double.infinity,
//               decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.circular(20),
//                 border: Border.all(color: Color(0xFFE5E5E5)),
//               ),
//               child: Row(
//                 children: <Widget>[
//                   SizedBox(
//                     width: 15,
//                   ),
//                   Icon(Icons.location_on_outlined),
//                   SizedBox(
//                     width: 20,
//                   ),
//                   Text("Location"),
//                   SizedBox(
//                     width: 170,
//                   ),
//                   Icon(Icons.keyboard_arrow_right_outlined),
//                 ],
//               ),
//             ),
//           ],
//         ));
//   }
// }